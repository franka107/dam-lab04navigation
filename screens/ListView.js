import React from 'react';
import {
  TouchableOpacity,
  Image,
  FlatList,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import database from '../database.json';

function CubeItem({
  navigation,
  uriImage,
  name,
  sku,
  price,
  shortDescription,
  item,
}) {
  let uriProcessed = uriImage.substr(0, uriImage.indexOf(','));
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Detalle', { shortDescription, item })}
      style={styles.itemContainer}>
      <View>
        <Image
          source={{ uri: uriProcessed || 'test' }}
          style={styles.mainImage}
        />
        <Text style={styles.itemTitle}>{name}</Text>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}>
          <Text style={styles.itemContent}>SKU: {sku}</Text>
          <Text style={styles.itemContent}>Precio: {price}</Text>
        </View>
        <Text style={[styles.itemDescription, { color: 'white' }]} numberOfLines={4}>
          {shortDescription}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export default class ListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      database,
    };
  }

  render() {
    return (
      <View >
        <FlatList
          data={database}
          renderItem={({ item }) => (
            <CubeItem
              uriImage={item.Imágenes}
              shortDescription={item['Descripción corta']}
              name={item.Nombre}
              sku={item.SKU}
              price={item['Precio rebajado']}
              navigation={this.props.navigation}
              item={item}
            />
          )}
          numColumns={2}
          keyExtractor={item => item.SKU}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    marginTop: 10,
    margin: 5,
    padding: 10,
    backgroundColor: '#fdfdfd',
    borderRadius: 5,
    elevation: 5,
    flex: 1,
    flexDirection: 'column',
  },
  itemTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12,
  },
  itemContent: {
    fontSize: 12,
    textAlign: 'center',
    width: '50%',
  },
  itemDescription: {
    fontSize: 10,
    textAlign: 'center',
    backgroundColor: '#03c2fb',
    borderRadius: 4,
    padding: 4,
    marginTop: 5,
    elevation: 5,
  },
  mainImage: {
    width: 170,
    height: 170,
  },
});
